import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { SearchbarComponent } from './searchbar/searchbar.component';
import { ViewpanelComponent } from './searchbar/viewpanel/viewpanel.component';

import { HttpClientModule } from '@angular/common/http';

//import FormsModule  to use ngModel for 2-way data-binding.
import {FormsModule} from '@angular/forms';
import { SearchService } from './common/search-service/search-service'
@NgModule({
  declarations: [
    AppComponent,
    SearchbarComponent,
    ViewpanelComponent,
   
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [SearchService],
  bootstrap: [AppComponent]
})
export class AppModule { }
