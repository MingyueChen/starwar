
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable()
export class SearchService {

  constructor(private httpClient:HttpClient) { }

  ngOnInit() {
  }

  getInfo(url: string): Observable<any[]> {
    return this.httpClient.get<any[]>(url);
  }

}
