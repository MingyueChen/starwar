import { Component, OnInit, Input } from '@angular/core';
import { People } from 'src/app/model/People';

@Component({
  selector: 'app-viewpanel',
  templateUrl: './viewpanel.component.html',
  styleUrls: ['./viewpanel.component.css']
})
export class ViewpanelComponent implements OnInit {
  @Input() showResult: any;
  @Input() select_category: string;
  constructor() { }

  ngOnInit() {
    
  }

}
