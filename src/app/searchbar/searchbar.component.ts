import { Component, OnInit, ErrorHandler } from '@angular/core';
import { FormGroup } from '@angular/Forms';
import { SearchService } from '../common/search-service/search-service';
import { HttpErrorResponse } from '@angular/common/http';
import { People } from '../model/People';

@Component({
  selector: 'app-searchbar',
  templateUrl: './searchbar.component.html',
  styleUrls: ['./searchbar.component.css']
})
export class SearchbarComponent implements OnInit {

  categories: Array<string> = [];
  object_ids: Array<number> = [];

  selected_category: string;
  selected_id: string;
  result: Array<any>;
 
  constructor(private ss: SearchService) {
    // create a category
    this.categories.push("people", "planets", "vehicles", "species", "starships");
    this.object_ids.push(1, 2, 3, 4, 5);
  }

  ngOnInit() {
  }

  submitItem(f : FormGroup)  {
    // this.fs = new FilmService();
    this.ss.getInfo("https://swapi.co/api/" + this.selected_category + "/" + this.selected_id + "/").subscribe(response => {
      this.result = response;
  } ,
    error => {
      console.log("the information is not found " + error + "  " + this.result );
    });
  
  
  }


}
